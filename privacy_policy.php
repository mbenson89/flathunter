<?php
    // First we execute our common code to connection to the database and start the session
    require("includes/common.php");
	
	$pageTitle = "Privacy Policy";
	$styles = array("page_styles/terms_of_use.css", "errorpages/styles.css");
	
	include 'includes/header.php'; 
	
?>
<h1>Privacy Policy</h1>
<hr>
<?php
	include 'errorpages/section_unavailable.php'; 
	include 'includes/footer.php'; 
?>