/**
 * @author Mark Benson
 */
function regionChange(){
	var regionSelector = document.getElementById('regionSelector');
	$.getJSON('/ajax/getdistricts.php',{region : regionSelector.options[regionSelector.selectedIndex].value}, function(e){
		districtUpdate(e[0].ID);
		var options = '';
		for(var i = 0; i < e.length; i++){
			options += '<option value="' + e[i].ID + '">' + e[i].Name + '</option>';
		}
		$('select#districtSelector').html(options);
	});	
}

function districtUpdate(districtID){
	$.getJSON('/ajax/getsuburbs.php',{district : districtID}, function(e){
		var options = '';
		for(var i = 0; i < e.length; i++){
			options += '<option value="' + e[i].ID + '">' + e[i].Name + '</option>';
		}
		$('select#suburbSelector').html(options);
	});
}

function geocode(){
	var suburbSelector = document.getElementById('suburbSelector');
	var suburbID = suburbSelector.options[suburbSelector.selectedIndex].value;
	var address = document.getElementById('address').value;
	
	//Display loading dialog and curtain
	//And set the dialog contents
	$("#curtain").css("visibility", "visible");
	$("#dialogIcon").html('<img src="/assets/ajaxload-location.png" alt="" width="128" height="128"/>');
	$("#dialogTitle").html("Locating the address...");
	$("#dialogMessage").html("Specifying the correct address is very important for FlatHunter to work correctly.");
	$("#dialogButtons").html("");
	
	$.getJSON('/bingservices/geocode.php', {address: address, suburb: suburbID}, function(e){
		var lat, lon, success, error;
		 $.each( e, function( key, val ) {
		 	if(key === "Success"){
		 		success = val;
		 	}
		 	
		 	if(key === "Error"){
		 		error = val;
		 	}
		 	
			if(key === "Lat"){
				lat = val;
			}
			else if (key === "Lon"){
				lon = val;
			}
			
			if(!success){
				//Notify the user that the address lookup failed by updating the dialog
				$("#dialogIcon").html('<img src="/assets/fail.png" alt="" width="128" height="128"/>');
				if(error === "Number"){
					$("#dialogTitle").html("The address is invalid!");
					$("#dialogMessage").html("Don't include Flat numbers/letters in the street address.<br>Click Close to try again.");
				}
				else if (error === "Address"){
					$("#dialogTitle").html("The address couldn't be found!");
					$("#dialogMessage").html("We couldn't find the address you specified. If this is incorrect, click <a href=\"javascript:void(0);\">here</a>, otherwise click Close to try again.");
				}
				else if (error === "Connection"){
					$("#dialogTitle").html("Oops!");
					$("#dialogMessage").html("There is a problem connecting to the Bing Maps service right now.<br>Please try again later.");
				}
				else if (error === "Invalid"){
					$("#dialogTitle").html("Oops!");
					$("#dialogMessage").html("There was an error with the generated query.<br>Please try again.");
				}
				
				$("#dialogButtons").html('<button type="button" value="Close" onclick="$(\'#curtain\').css(\'visibility\', \'hidden\')";>Close</button>');
			}
			else{
				$("input#coords").val(lat+','+lon);
				
				//Notify the user that the address lookup was successful by updating the dialog
				$("#dialogIcon").html('<img src="/assets/success.png" alt="" width="128" height="128"/>');
				$("#dialogTitle").html("Address Found!");
				$("#dialogMessage").html("Click continue to move to the next step.");
				$("#dialogButtons").html('<button type="submit" value="Continue">Continue</button>');
			}
		});
	});
}
