<?php
    require ("../includes/common.php");	
	require ("../dbAccess.php");
	
    if(!isset($_SESSION['user'])){
        header("Location: /login.php?return=%2Fnewlisting%2Fstep1.php");
        die("Redirecting to: login.php");
    }

	$pageTitle = "Create Listing : Step 1";
	$styles = array("/page_styles/newlisting.css");
	$scripts = array("http://code.jquery.com/jquery-1.10.1.min.js", "step1.js");
	include ("../includes/header.php");
	
	if(isset($_POST['coords'])){
		$_SESSION['listing'] = array(
			'address' => $_POST['addressLine'],
			'suburb' => $_POST['suburb'],
			'coords' => $_POST['coords'],
			'showAddress' => $_POST['displayAddress']
		);
		
		header("Location: step2.php");
        die("Redirecting to: step2.php");
	}
?>
<h1>Create Listing</h1>
<hr>
<h2>Step 1: Address</h2>

<form method="post">
	<input type="hidden" id="coords" name="coords"/>
	<table>
		<tr>
			<td>Street Address:</td>
			<td><input type="text" id="address" name="addressLine" value="" /></td>
		</tr>
		<tr>
			<td>Display full address:</td>
			<td>
				<select name="displayAddress">
					<option value="false">No</option>
					<option value="true">Yes</option>		
				</select>
			</td>
		</tr>
		<tr>
			<td>Region:</td>
			<td>
				<select id="regionSelector" name="region" onchange="regionChange();">
					<?php
						$regions = getRegions();
						foreach($regions as $region){
							echo "<option value=\"{$region["ID"]}\">{$region["Name"]}</option>\n";
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>District:</td>
			<td>
				<select id="districtSelector" name="district" onchange="districtUpdate(document.getElementById('districtSelector').options[districtSelector.selectedIndex].value);">
					<?php
						$districts = getDistrictsForRegion(1);
						foreach($districts as $district){
							echo "<option value=\"{$district["ID"]}\">{$district["Name"]}</option>\n";
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Suburb:</td>
			<td>
				<select id="suburbSelector" name="suburb">
					<?php
						$suburbs = getSuburbsForDistrict(1);
						foreach($suburbs as $suburb){
							echo "<option value=\"{$suburb["ID"]}\">{$suburb["Name"]}</option>\n";
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><div style="text-align: right"><button type="button" value="Continue" onclick="geocode();"/>Continue</button></span></td>
		</tr>
		<div id="curtain">
			<div id="dialog">
				<div id="dialogIcon"></div>
				<div id="dialogTitle"></div>
				<div id="dialogMessage"></div>
				<div id="dialogButtons"></div>
			</div>
		</div>
	</table>
</form>

<?php
	include ("../includes/footer.php");
?>
