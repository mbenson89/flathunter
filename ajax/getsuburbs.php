<?php
    if(isset($_GET['district'])){
    	require ("../includes/common.php");
		require ("../dbAccess.php");
		
		$suburbs = getSuburbsForDistrict($_GET['district']);
		
		echo(json_encode($suburbs));
    }
	else {
		die('E: No district specified');
	}
?>