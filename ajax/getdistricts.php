<?php
    if(isset($_GET['region'])){
    	require ("../includes/common.php");
		require ("../dbAccess.php");
		
		$districts = getDistrictsForRegion($_GET['region']);
		
		echo(json_encode($districts));
    }
	else {
		die('E: No region specified');
	}
?>