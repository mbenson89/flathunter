<?php
    // First we execute our common code to connection to the database and start the session
    require("includes/common.php");
	
	$pageTitle = "Terms of Use";
	$styles = array("page_styles/terms_of_use.css", "errorpages/styles.css");
	
	include 'includes/header.php'; 
	
?>
<h1>Terms of Use</h1>
<hr>
<?php
	include 'errorpages/section_unavailable.php'; 
	include 'includes/footer.php'; 
?>