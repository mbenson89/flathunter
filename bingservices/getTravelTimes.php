<?php
    require('../includes/common.php');
	require('../dbAccess.php');
	if(isset($_GET['listing']) && isset($_GET['user'])){
		
		//Retrieve the coordinates of the destinations (up to five)
		$dests  = Array(
			Array(
				"Description" => "Uni",
				"Location" => Array(
								"Latitude" => -41.279062,
								"Longitude" => 174.778851)
			),
			Array(
				"Description" => "Stafford House",
				"Location" => Array(
								"Latitude" => -41.280470,
								"Longitude" => 174.775243)
			)
		);
		
		//Retrieve the coordinates of the listing
		$listingCoords = Array("Location" => Array("Latitude" => -41.2945518, "Longitude" => 174.7627258));
		
		if(!(isset($dests) && count($dests) > 0 && isset($listingCoords))){
			die("E: Unable to retrieve places of interest for user or listing location");
		}
		
		if(count($dests) > 5){
			die("E: Too many destinations!");
		}
		
		//The Bing API key
		$bingApiKey = "Akf36tQgCRhEQFsUDciUorp2QapNHHSy-qkaO_9qIvqCHjlXBY0lAOGQPob5JYGc";
		//Create a credentials 'object' to be included as a parameter in requests
		$credentials = array('ApplicationId' => $bingApiKey);
		
		$routeServiceWsdl = "routeservice.wsdl";
		
		$routeClient = new SoapClient($routeServiceWsdl, array('trace' => 1));
		
		print "{\n";
		foreach ($dests as $dest) {
			$waypoints = Array($listingCoords, $dest);
			
			// Construct the Route request objects for the drive route and walking route
			$driveRouteRequest = array(
			  'Credentials' => $credentials,
			  'Waypoints' => $waypoints,
			  'Options' => Array("Mode" => "Driving", "RoutePathType" => "None")
			);
			
			$walkRouteRequest = array(
				'Credentials' => $credentials,
			  'Waypoints' => $waypoints,
			  'Options' => Array("Mode" => "Walking", "RoutePathType" => "None")
			);
			
			//Calculate the routes
			$driveRoute = calculateRoute($routeClient, $driveRouteRequest);
			$walkRoute = calculateRoute($routeClient, $walkRouteRequest);
			
			//Retrieve the drive and walk times
			$driveTime = $driveRoute->CalculateRouteResult->Result->Summary->TimeInSeconds;
			$walkTime = $walkRoute->CalculateRouteResult->Result->Summary->TimeInSeconds;
			
			//Convert the drive and walk times to minutes, and round to the nearest 5 minutes
			$driveTime = $driveTime / 60;
			$driveTime = roundToAny($driveTime);
			
			$walkTime = $walkTime / 60;
			$walkTime = roundToAny($walkTime);
			
			//print them to the page
			print "\t{\n\t\t\"Description\": \"{$dest["Description"]}\"\n\t\t\"DriveTime\": \"{$driveTime} Minutes\"\n\t\t\"WalkTime\": \"{$walkTime} Minutes\"\n\t}\n";
		}
		print "}";
	}
	else{
		die('E: Arguments either missing or incorrect!');
	}
	
	
	function roundToAny($n,$x=5) {
	    return round($n/$x)*$x;
	}
	
	
	function calculateRoute($routeClient, $routeRequest) {
		try {
			$routeResponse = $routeClient->CalculateRoute(array('request' => $routeRequest));
		}
		catch(SoapFault $e)
		{
			die('Fault occurred using Web Service: '.$e->getMessage());
		}
		
		return $routeResponse;
	}
