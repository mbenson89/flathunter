<?php
	require('../includes/common.php');
	require('../dbAccess.php');
	if(isset($_GET['address']) && isset($_GET['suburb'])){
		
		$inputAddressParts = explode(" ", $_GET['address']);
		
		if(!stringIsNumber($inputAddressParts[0]))
		{
			$obj = array('Success' => false, 'Error' => 'Number');
			die(json_encode($obj));
		}
		
		//The Bing API key
		$bingApiKey = "Akf36tQgCRhEQFsUDciUorp2QapNHHSy-qkaO_9qIvqCHjlXBY0lAOGQPob5JYGc";
		//Create a credentials 'object' to be included as a parameter in requests
		$credentials = array('ApplicationId' => $bingApiKey);
	
		$geocodeServiceWsdl = "geocodeservice.wsdl";
	
		$geocodeClient = new SoapClient($geocodeServiceWsdl, array('trace' => 1));
		
		$suburb = getSuburbInfo($_GET['suburb']);
	
		//Get the region, district and suburb names
		$regionName = $suburb["Region"]["Name"];
		$districtName = $suburb["District"]["Name"];
		$suburbName = $suburb["Suburb"]["Name"];
		
		$address = $_GET['address'] . ", " . $suburbName . ", " . $districtName . " District, " .$regionName;
	
		// Construct the request
		$geocodeRequest = array(
			'Credentials' => $credentials,
			'Query' => $address
		);
		
		try {
			$geocodeResponse = $geocodeClient->Geocode(array('request' => $geocodeRequest));
		}
		catch(SoapFault $e)
		{
			$obj = array('Success' => false, 'Error' => 'Connection');
			die(json_encode($obj));
		}
		
		// Ouput geocoded latitude and longitude of provided address
		if (is_array($geocodeResponse->GeocodeResult->Results->GeocodeResult)) {
		  if (is_array($geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
		   Locations->GeocodeLocation)) {
			$lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
			 Locations->GeocodeLocation[0]->Latitude;
			$lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
			 Locations->GeocodeLocation[0]->Longitude;
			$adr = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
			 Address->FormattedAddress;
		  }
		  else {
			$lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
			 Locations->GeocodeLocation->Latitude;
			$lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
			 Locations->GeocodeLocation->Longitude;
			$adr = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
			 Address->FormattedAddress;
		  }
		}
		else
		{
		  $lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult->
		   Locations->GeocodeLocation[0]->Latitude;
		  $lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult->
		   Locations->GeocodeLocation[0]->Longitude;
		  $adr = $geocodeResponse->GeocodeResult->Results->GeocodeResult->
		   Address->FormattedAddress;
		}
		
		if(!isset($lat) || !isset($lon) || !isset($adr)){
			$obj = array('Success' => false, 'Error' => 'Address');
			die(json_encode($obj));
		}
		
		$outputAddressParts = explode(" ", $adr);
		
		//Check the input address matches the output address.
		//We only want to check the number, as theoretically the street name should be the same
		if(strcmp($inputAddressParts[0], $outputAddressParts[0]) != 0)
		{
			$obj = array('Success' => false, 'Error' => 'Address');
			die(json_encode($obj));
		}
		
		$coords = array('Success' => true, 'Lat' => $lat, 'Lon' => $lon);
		
		echo(json_encode($coords));
	}
	else{
		$obj = array('Success' => false, 'Error' => 'Invalid');
		die(json_encode($obj));
	}
	
	function stringIsNumber($string){
		$length = strlen($string);
		for ($i = 0; $i < $length; $i++) {
			if (!is_numeric($string[$i]))
				return false;
		}
		return true;
	}
?>
