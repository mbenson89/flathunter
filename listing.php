<?php
    // First we execute our common code to connection to the database and start the session
    require("includes/common.php");
	
 	$pageTitle = "Aro Valley (4 Bedrooms)";
	$styles = array("page_styles/listing.css", "errorpages/styles.css");

 	include 'includes/header.php'; 
?>
<div id="breadcrumbs">
	<a href="index.php">Home</a> > <a href="flatmates.php">Flatmates Wanted</a> > <a href="javascript:void(0);">Wellington</a> > <a href="javascript:void(0);">Wellington</a> > <a href="javascript:void(0);">Aro Valley</a> > Listing
</div>
<div id="listingHeader">
	<h1><?php echo $pageTitle ?> - $135 per week</h1>
	<div id="listingProperties"><span class="listingStatus closed"></span> | Listing ID: 0000001</div>
</div>
<hr>
<!--<div id="ownerOptions">
	<div id="ownerOptionsTitle">Listing options <span id="ownerOptionsSub">Only you can see this</span></div>
	<div id="trafficLight">
		<a href="javascript:void(0);" title="Close Listing"><div class="trafficLight" id="trafficLight_red"><div class="trafficLightOverlay_on"></div></div></a>
		<a href="javascript:void(0);" title="Temporarily close listing"><div class="trafficLight" id="trafficLight_orange"><div class="trafficLightOverlay_off"></div></div></a>
		<a href="javascript:void(0);" title="Open Listing"><div class="trafficLight" id="trafficLight_green"><div class="trafficLightOverlay_off"></div></div></a>
	</div>
</div>-->
<div class="messagebg HighPriority">
	<div class="flagmessage">This user has been reported for phishing. Be careful when dealing with this person.</div>
</div>
<div id="listing">
	<div id="listingSidebar">
		<div>
			<a href="javascript:void(0);" class="watchlistButton wlAdd"></a>
		</div>
		<div class="sidebarPanel">
			<div id="mainPhoto"></div>
			<div id="otherPhotosPanel">
				<div style="width: 385px;">
					<div class="otherPhoto"></div>
					<div class="otherPhoto"></div>
					<div class="otherPhoto"></div>
					<div class="otherPhoto"></div>
					<div class="otherPhoto"></div>
				</div>
			</div>
			<hr>
			<div id="map"><iframe width="280" height="200" frameborder="0" src="http://www.bing.com/maps/embed/viewer.aspx?v=3&cp=-41.294503~174.762771&lvl=15&w=280&h=200&sty=r&typ=s&pp=&ps=55&dir=0&mkt=en-us&form=BMEMJS"></iframe></div>
		</div>
		<div class="sidebarPanel">
			<b>Contact Details</b>
			<hr>
			<div class="messagebg MediumPriority">
				<div class="hintmessage">
					This listing has been temporarily closed by the lister.<br>Add the listing to your watchlist to be notified if it reopens.
				</div>
			</div>
			<div style="margin-left: auto; margin-right: auto; width:100px;">
				<a href="javascript:void(0);" class="watchlistButton wlAdd"></a>
			</div>
			<!--<div id="contactDetails">
				Call or text <a href="tel:021 265 5563">021 265 5563</a>, or email <a href="mailto:markbenson@outlook.com">markbenson@outlook.com</a>
			</div>-->
		</div>
		<div>
			<div id="flag"><a href="javascript:void(0);"><img src="assets/flag.png" alt=""/> Report this listing</a></div>
		</div>
	</div>
	<div id="listingContent">
		<div class="contentItem">
			<div class="contentTitle">Location</div>
			<div class="contentInfo">Aro Valley,<br>Wellington,<br>Wellington</div>
		</div>
		<div class="contentItem">
			<div class="contentTitle">Available</div>
			<div class="contentInfo">Now until December 27 2013 - Negotiable</div>
		</div>
		<div class="contentItem">
			<div class="contentTitle">Description</div>
			<div class="contentInfo">
				<div id="bedbathcar"><img src="assets/bed.png" alt="" width="32" height="32"/> <b>4</b> Bedrooms | <img src="assets/bath.png" alt="" width="32" height="32"/> <b>2</b> Bathrooms | <img src="assets/car.png" alt="" width="32" height="32"/> <b>0</b> Offstreet Parks</div>
				<hr>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras gravida porta turpis, vel commodo urna tristique id. Aenean in lacinia nibh, sit amet rhoncus magna. Vivamus pulvinar ultrices mi eget semper. Etiam aliquet neque at tincidunt tempus. Quisque in velit consectetur, sodales purus varius, pharetra erat. Donec varius nisi non urna auctor, a tempor odio imperdiet. Nunc non magna in erat consequat tempus non sit amet orci.
			</div>
		</div>
		<div class="contentItem">
			<div class="contentTitle">Furnishings</div>
			<div class="contentInfo">
				The flat is fully furnished, but the room is unfurnished.
			</div>
		</div>
		<div class="contentItem">
			<div class="contentTitle">Cost Overview</div>
			<div class="contentInfo">
					<div class="contentSubTitle">Initial:</div>
					<div class="contentSubInfo">2 weeks Bond + 2 weeks rent in advance</div>
					<div class="contentSubTitle">Ongoing:</div>
					<div class="contentSubInfo">$25 per week - covers power, internet, Sky TV, TP and cleaning products</div>
			</div>
		</div>
		<div class="contentItem">
			<div class="contentTitle">Places of Interest</div>
			<div class="contentInfo">
				<!--<table>
					<tr>
						<th>Location</th>
						<th class="poi_timeCol">Drive</th>
						<th class="poi_timeCol">Walk</th>
					</tr>
					<tr>
						<td>Courtenay Place</td>
						<td>5 Minutes</td>
						<td>23 Minutes</td>
					</tr>
					<tr>
						<td>Stafford House</td>
						<td>5 Minutes</td>
						<td>26 Minutes</td>
					</tr>
					<tr>
						<td>Lambton Quay</td>
						<td>6 Minutes</td>
						<td>26 Minutes</td>
					</tr>
					<tr>
						<td>Victoria (Pipitea)</td>
						<td>8 Minutes</td>
						<td>32 Minutes</td>
					</tr>
				</table>
				<div class="messagebg">
					<div class="hintmessage">These times are estimates and are calculated for travel <strong>from</strong> the flat.<br>Traffic, one-way streets and other factors may reduce the accuracy of the quoted times.</div>
				</div>-->
				<div class="messagebg LowPriority">
					<div class="hintmessage">
						<h1>This section is only available when you're logged in</h1>
						With a free account you can see the travel time from this flat to some of the most important places to you!<br>
						<a href="login.php">Log in</a> or <a href="register.php">Sign Up</a> - it only takes a minute!
					</div>
				</div>
			</div>
		</div>
		<div class="contentItem">
			<div class="contentTitle">In the Area</div>
			<div class="contentInfo">
				<div class="messagebg LowPriority">
					<div class="hintmessage">
						<h1>This section is coming soon</h1>
						When released, you will be able to see nearby landmarks and public transit stops.
					</div>
				</div>
			</div>
		</div>
		<div class="contentItem">
			<div class="contentTitle">Flatmates</div>
			<div class="contentInfo">
				<b>4</b> current flatmates; <img src="assets/male.png" width="16" height="16"/> <b>3</b> guys, <img src="assets/female.png" width="16" height="16"/> <b>1</b> girl. Aged 21 - 23.
				<hr>
				Mauris et consectetur mauris. Duis tempus metus at ornare rutrum. In vel faucibus dolor. Morbi at luctus nunc, quis consequat.
			</div>
		</div>
		<div class="contentItem">
			<div class="contentTitle">Ideal Flatmate</div>
			<div class="contentInfo">
					<div class="contentSubTitle">Requirements:</div>
					<div class="contentSubInfo">Must be a girl between ages 20 and 25.</div>
					<div class="contentSubTitle">We Prefer:</div>
					<div class="contentSubInfo">Someone who would like to join in with communal cooking.</div>
			</div>
		</div>
	</div>
	<div id="listingFooter">
			<span id="viewCounter">0007</span><br>Page Views
		</div>
</div>
<?php include 'includes/footer.php'; ?>
