 <?php

    // First we execute our common code to connection to the database and start the session
    require("includes/common.php");
    
    // We remove the user's data from the session
    unset($_SESSION['user']);
    
    //Redirect to the homepage
	//TODO: Update this with the actual host URL
	$RedirectURL = "/";
		
    // We redirect them to the login page
    header("Location: " . $RedirectURL);
    die("Redirecting to: " . $RedirectURL); 
