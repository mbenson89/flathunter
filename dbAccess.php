<?php
function getRegionName($rID){
	//Use the global db variable
	global $db;
	
	$query = "
		SELECT
			RegionName
		FROM
			Region
		WHERE
			RegionID = :rID
	";
	
	$query_params = array(
		":rID" => $rID
	);
	
	try
	{
		// Execute the query against the database
		$stmt = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch(PDOException $ex)
	{
		// Note: On a production website, you should not output $ex->getMessage().
		// It may provide an attacker with helpful information about your code. 
		die("Failed to run query: " . $ex->getMessage());
	}
	
	$row = $stmt->fetch();
	
	return $row['RegionName'];
}

function getDistrictName($dID){
	//Use the global db variable
	global $db;
	
	$query = "
		SELECT
			DistrictName
		FROM
			District
		WHERE
			DistrictID = :dID
	";
	
	$query_params = array(
		":dID" => $dID
	);
	
	try
	{
		// Execute the query against the database
		$stmt = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch(PDOException $ex)
	{
		// Note: On a production website, you should not output $ex->getMessage().
		// It may provide an attacker with helpful information about your code. 
		die("Failed to run query: " . $ex->getMessage());
	}
	
	$row = $stmt->fetch();
	
	return $row['DistrictName'];
}

function getSuburbInfo($sID){
	//Use the global db variable
	global $db;
	
	$query = "
		SELECT
			SuburbName, SuburbID, DistrictName, District.DistrictID AS DistrictID, RegionName, Region.RegionID AS RegionID
		FROM
			Suburb, District, Region
		WHERE
			Suburb.DistrictID = District.DistrictID AND
			District.RegionID = Region.RegionID AND
			SuburbID = :sID
	";
	
	$query_params = array(
		":sID" => $sID
	);
	
	try
	{
		// Execute the query against the database
		$stmt = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch(PDOException $ex)
	{
		// Note: On a production website, you should not output $ex->getMessage().
		// It may provide an attacker with helpful information about your code. 
		die("Failed to run query: " . $ex->getMessage());
	}	
	
	$row = $stmt->fetch();
	
	return array(
		"Suburb" => array('Name' => $row['SuburbName'], 'ID' => $row['SuburbID']),
		"District" => array('Name' => $row['DistrictName'], 'ID' => $row['DistrictID']),
		"Region" => array('Name' => $row['RegionName'], 'ID' => $row['RegionID'])
	);
}

function getDistrictInfo($dID){
	//Use the global db variable
	global $db;
	
	$query = "
		SELECT
			DistrictName, DistrictID, RegionName, Region.RegionID AS RegionID
		FROM
			District, Region
		WHERE
			District.RegionID = Region.RegionID AND
			DistrictID = :dID
	";
	
	$query_params = array(
		":dID" => $dID
	);
	
	try
	{
		// Execute the query against the database
		$stmt = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch(PDOException $ex)
	{
		// Note: On a production website, you should not output $ex->getMessage().
		// It may provide an attacker with helpful information about your code. 
		die("Failed to run query: " . $ex->getMessage());
	}	
	
	$row = $stmt->fetch();
	
	return array(
		"District" => array('Name' => $row['DistrictName'], 'ID' => $row['DistrictID']),
		"Region" => array('Name' => $row['RegionName'], 'ID' => $row['RegionID'])
	);
}

function getSuburbsForDistrict($district){
	//Use the global db variable
	global $db;
	
	$query = "
		SELECT
			SuburbName, SuburbID
		FROM
			Suburb
		WHERE
			Suburb.DistrictID = :dID
	";
	
	$query_params = array(
		":dID" => $district
	);
	
	try
	{
		// Execute the query against the database
		$stmt = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch(PDOException $ex)
	{
		// Note: On a production website, you should not output $ex->getMessage().
		// It may provide an attacker with helpful information about your code. 
		die("Failed to run query: " . $ex->getMessage());
	}
	
	while ($row = $stmt->fetch()) {
        if(!isset($suburbs)){
        	$suburbs = array(array('Name' => $row['SuburbName'], 'ID' => $row['SuburbID']));
        }
		else{
			$suburbs[] = array('Name' => $row['SuburbName'], 'ID' => $row['SuburbID']);
		}
    }

	return $suburbs;
}

function getDistrictsForRegion($region){
	//Use the global db variable
	global $db;
	
	$query = "
		SELECT
			DistrictName, DistrictID
		FROM
			District
		WHERE
			District.RegionID = :rID
	";
	
	$query_params = array(
		":rID" => $region
	);
	
	try
	{
		// Execute the query against the database
		$stmt = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch(PDOException $ex)
	{
		// Note: On a production website, you should not output $ex->getMessage().
		// It may provide an attacker with helpful information about your code. 
		die("Failed to run query: " . $ex->getMessage());
	}
	
	while ($row = $stmt->fetch()) {
        if(!isset($districts)){
        	$districts = array(array('Name' => $row['DistrictName'], 'ID' => $row['DistrictID']));
        }
		else{
			$districts[] = array('Name' => $row['DistrictName'], 'ID' => $row['DistrictID']);
		}
    }

	return $districts;
}

function getRegions(){
	//Use the global db variable
	global $db;
	
	$query = "
		SELECT
			RegionName, RegionID
		FROM
			Region
	";
	
	try
	{
		// Execute the query against the database
		$stmt = $db->prepare($query);
		$result = $stmt->execute();
	}
	catch(PDOException $ex)
	{
		// Note: On a production website, you should not output $ex->getMessage().
		// It may provide an attacker with helpful information about your code. 
		die("Failed to run query: " . $ex->getMessage());
	}
	
	while ($row = $stmt->fetch()) {
        if(!isset($regions)){
        	$regions = array(array('Name' => $row['RegionName'], 'ID' => $row['RegionID']));
        }
		else{
			$regions[] = array('Name' => $row['RegionName'], 'ID' => $row['RegionID']);
		}
    }

	return $regions;
}
