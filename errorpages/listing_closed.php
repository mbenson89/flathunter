<div id="errorSection">
	<img src="errorpages/closedicon.png" alt="Closed" width="128" height="116" id="errorImage">
	
	<div id="errorHeading">
		This listing is closed!
	</div>
	
	<div id="errorMessage">
		Sorry, the listing you requested is closed.
	</div>
</div>