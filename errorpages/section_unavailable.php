<div id="errorSection">
	<img src="errorpages/errorIcon.png", width="128" height="128" id="errorImage">
	
	<div id="errorHeading">
		Section Unavailable!
	</div>
	
	<div id="errorMessage">
		Sorry, this section is currently unavailable. We are working on it and it should be ready soon!
	</div>
</div>
