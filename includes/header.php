<!DOCTYPE html>
<head>
<meta charset="UTF-8"> 
<?php
echo "<title>{$pageTitle} - FlatHunter</title>\n";

//If there are no styles set, create $styles containing the default CSS file path 
if(!isset($styles)){
	$styles = array("/styles.css");
}
//If there are preexisting styles, append the default CSS file path to the array
else{
	$styles[] = "/styles.css";
}

//Add all the styles declarations to the html file
foreach($styles as $i){
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$i}\">\n";
}

//If there are any scripts defined, add them to the html file
if(isset($scripts)){
	foreach($scripts as $i){
		echo "<script src=\"{$i}\"></script>\n";
	}
}
?>
<link rel="icon" type="image/png" href="/favicon.png" />
</head>
<body>
<div id="alignmentContainer">
	<div id="pageBorder">
		<div id="contentContainer">
			<header>
				<div id="headerimg"></div>
				<div id="userpane">
					<?php
						echo "<div id=\"welcomeText\">";
						echo "Welcome";
						if(!empty($_SESSION['user']))
						{
							echo ",&nbsp;<b>" . htmlentities($_SESSION['user']['firstname'], ENT_QUOTES, 'UTF-8') . "</b>";
						}
						echo "</div>\n";
					
						echo "\t\t\t\t\t<div id=\"userOptions\">";
						if(!empty($_SESSION['user']))
						{
							echo "<a href=\"/edit_account.php\">Account</a> | <a href=\"javascript:void(0);\">Watchlist</a> | <a href=\"/logout.php\">Log out</a>";
						}
						else 
						{
							echo "<a href=\"/login.php\">Log in</a> | <a href=\"/register.php\">Sign up</a>";
						}
						
						echo "</div>\n";
					?>
				</div>
			</header>
			<nav>
				<a href="/" class="primaryNavItem">Home</a><a href="/catalog.php?type=flatmates" class="primaryNavItem">Flatmates Wanted</a><a href="/catalog.php?type=rentals" class="primaryNavItem">Rental Properties</a><span id="secondaryNavBG"><a href="/newlisting/step1.php" class="secondaryNavItem">Add Listing</a></span>
			</nav>
			
