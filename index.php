<?php
    // First we execute our common code to connection to the database and start the session
    require("includes/common.php");
	
 	$pageTitle = "Home";

 	include 'includes/header.php'; 
?>
<div>
<h1>Welcome to FlatHunter!</h1>
<hr>
<p>
	I built this website to make it easier to find your perfect flat.<br>
	We do this by providing heaps of amazing features which make the sometimes frustrating task of finding a flat just that little bit easier.
</p>
<p>
	FlatHunter is currently in BETA, and for a limited time <b>all listings are FREE!</b> (will be $8.00).
</p>
</div>
<?php include 'includes/footer.php'; ?>
