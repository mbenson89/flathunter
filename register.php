<?php

    // First we execute our common code to connection to the database and start the session
    require("includes/common.php");
	
	$dateIsValid = true;
	$age = 16;
	$usernameValid = true;
	$passwordIsValid = true;
	$passwordsMatch = true;
	$emailIsValid = true;
	
    // This if statement checks to determine whether the registration form has been submitted
    // If it has, then the registration code is run, otherwise the form is displayed
    if(!empty($_POST))
    {
        // Ensure that the user has entered a non-empty username
        if(empty($_POST['username']))
        {
            $usernameValid = false;
        }
        
        // Ensure that the user has entered a non-empty password
        if(empty($_POST['password']))
        {
            $passwordIsValid = false;
        }
		
		if($_POST['password'] == $_POST['passwordcheck'])
		{
			$passwordsMatch = false;
		}
        
		$dateIsValid = checkdate($_POST['birthmonth'], $_POST['birthday'], $_POST['birthyear']);
		
		$age = (date("md", date("U", mktime(0, 0, 0, $_POST['birthmonth'], $_POST['birthday'], $_POST['birthyear']))) > date("md") ? ((date("Y")-$_POST['birthyear'])-1):(date("Y")-$_POST['birthyear']));
		
		if($age < 0)
		{
			$dateIsValid = false;
		}
		
		// Make sure the user entered a valid E-Mail address
        // filter_var is a useful PHP function for validating form input, see:
        // http://us.php.net/manual/en/function.filter-var.php
        // http://us.php.net/manual/en/filter.filters.php
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        {
            $emailIsValid = false;
        }
		
		if($dateIsValid && $age >= 16 && $passwordIsValid && $passwordsMatch && $emailIsValid){	        
	        // We will use this SQL query to see whether the username entered by the
	        // user is already in use.  A SELECT query is used to retrieve data from the database.
	        // :username is a special token, we will substitute a real value in its place when
	        // we execute the query.
	        $query = "
	            SELECT
	                1
	            FROM users
	            WHERE
	                username = :username
	        ";
	        
	        // This contains the definitions for any special tokens that we place in
	        // our SQL query.  In this case, we are defining a value for the token
	        // :username.  It is possible to insert $_POST['username'] directly into
	        // your $query string; however doing so is very insecure and opens your
	        // code up to SQL injection exploits.  Using tokens prevents this.
	        // For more information on SQL injections, see Wikipedia:
	        // http://en.wikipedia.org/wiki/SQL_Injection
	        $query_params = array(
	            ':username' => $_POST['username']
	        );
	        
	        try
	        {
	            // These two statements run the query against your database table.
	            $stmt = $db->prepare($query);
	            $result = $stmt->execute($query_params);
	        }
	        catch(PDOException $ex)
	        {
	            // Note: On a production website, you should not output $ex->getMessage().
	            // It may provide an attacker with helpful information about your code. 
	            die("Failed to run query: " . $ex->getMessage());
	        }
	        
	        // The fetch() method returns an array representing the "next" row from
	        // the selected results, or false if there are no more rows to fetch.
	        $row = $stmt->fetch();
	        
	        // If a row was returned, then we know a matching username was found in
	        // the database already and we should not allow the user to continue.
	        if($row)
	        {
	            die("This username is already in use");
	        }
	        
	        // Now we perform the same type of check for the email address, in order
	        // to ensure that it is unique.
	        $query = "
	            SELECT
	                1
	            FROM users
	            WHERE
	                email = :email
	        ";
	        
	        $query_params = array(
	            ':email' => $_POST['email']
	        );
	        
	        try
	        {
	            $stmt = $db->prepare($query);
	            $result = $stmt->execute($query_params);
	        }
	        catch(PDOException $ex)
	        {
	            die("Failed to run query: " . $ex->getMessage());
	        }
	        
	        $row = $stmt->fetch();
	        
	        if($row)
	        {
	            die("This email address is already registered");
	        }
	        
	        // An INSERT query is used to add new rows to a database table.
	        // Again, we are using special tokens (technically called parameters) to
	        // protect against SQL injection attacks.
	        $query = "
	            INSERT INTO users (
	                username,
	                password,
	                salt,
	                email
	            ) VALUES (
	                :username,
	                :password,
	                :salt,
	                :email
	            )
	        ";
	        
	        // A salt is randomly generated here to protect again brute force attacks
	        // and rainbow table attacks.  The following statement generates a hex
	        // representation of an 8 byte salt.  Representing this in hex provides
	        // no additional security, but makes it easier for humans to read.
	        // For more information:
	        // http://en.wikipedia.org/wiki/Salt_%28cryptography%29
	        // http://en.wikipedia.org/wiki/Brute-force_attack
	        // http://en.wikipedia.org/wiki/Rainbow_table
	        $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));
	        
	        // This hashes the password with the salt so that it can be stored securely
	        // in your database.  The output of this next statement is a 64 byte hex
	        // string representing the 32 byte sha256 hash of the password.  The original
	        // password cannot be recovered from the hash.  For more information:
	        // http://en.wikipedia.org/wiki/Cryptographic_hash_function
	        $password = hash('sha256', $_POST['password'] . $salt);
	        
	        // Next we hash the hash value 65536 more times.  The purpose of this is to
	        // protect against brute force attacks.  Now an attacker must compute the hash 65537
	        // times for each guess they make against a password, whereas if the password
	        // were hashed only once the attacker would have been able to make 65537 different 
	        // guesses in the same amount of time instead of only one.
	        for($round = 0; $round < 65536; $round++)
	        {
	            $password = hash('sha256', $password . $salt);
	        }
	        
	        // Here we prepare our tokens for insertion into the SQL query.  We do not
	        // store the original password; only the hashed version of it.  We do store
	        // the salt (in its plaintext form; this is not a security risk).
	        $query_params = array(
	            ':username' => $_POST['username'],
	            ':password' => $password,
	            ':salt' => $salt,
	            ':email' => $_POST['email']
	        );
	        
	        try
	        {
	            // Execute the query to create the user
	            $stmt = $db->prepare($query);
	            $result = $stmt->execute($query_params);
	        }
	        catch(PDOException $ex)
	        {
	            // Note: On a production website, you should not output $ex->getMessage().
	            // It may provide an attacker with helpful information about your code. 
	            die("Failed to run query: " . $ex->getMessage());
	        }
	        
	        // This redirects the user back to the login page after they register
	        header("Location: index.php");
	        
	        // Calling die or exit after performing a redirect using the header function
	        // is critical.  The rest of your PHP script will continue to execute and
	        // will be sent to the user if you do not die or exit.
	        die("Redirecting to index.php");
        }
    }

	//If a return URL is provided, we will use that
	if(isset($_GET['return'])){
		$RedirectURL = $_GET['return'];
	}
	//If there is no return URL provided, we will return to the referrer page
	else if(isset($_SERVER['HTTP_REFERER'])){
		$RedirectURL = $_SERVER['HTTP_REFERER'];
	}
	//If there is no referrer page we will return to the homepage
	else{
		//TODO: Update this with the actual host URL
		$RedirectURL = "http://localhost/";
	}
	 
	
	$pageTitle = "Sign Up";
	$styles = array("page_styles/register.css");
	
	include 'includes/header.php'; 
	
?>
<div id="registerForm">
	<div id="registerFormHeader">Sign Up to FlatHunter</div>
	<div id="registerFormContent">
		<form action="register.php" method="post">
			<input type="hidden" name="returnurl" value="<?php echo $RedirectURL; ?>" />
			<table>
				<tr>
					<td>Username:</td>
					<td><input type="text" name="username" value="<?php echo isset($_POST['username']) ? htmlspecialchars($_POST['username']) : ''; ?>" /></td>
				</tr>
				<?php
					if(!$usernameValid){
						echo "<tr>\n";
						echo "<td/>";
						echo "<td><span class=\"errormsg\">You must enter a username!</span></td>\n";
						echo "</tr>\n";
					}
				?>
				<tr>
					<td>First Name:</td>
					<td><input type="text" name="firstname" value="<?php echo isset($_POST['firstname']) ? htmlspecialchars($_POST['firstname']) : ''; ?>" /></td>
				</tr>
				<tr>
					<td>Last Name:</td>
					<td><input type="text" name="lastname" value="<?php echo isset($_POST['lastname']) ? htmlspecialchars($_POST['lastname']) : ''; ?>" /></td>
				</tr>
				<tr>
					<td>Date of Birth:</td>
					<td>
						<select name="birthday">
							<?php
								for($i = 1; $i <= 31; $i++){
									echo "<option value=\"{$i}\">{$i}</option>\n";
								}
							?>
						</select>
						<select name="birthmonth">
							<?php
								for($i = 1; $i <= 12; $i++){
									$monthName = date("F", mktime(0, 0, 0, $i, 10));
									echo "<option value=\"{$i}\">{$monthName}</option>\n";
								}
							?>
						</select>
						<select name="birthyear">
							<?php
								$curyear = intval(date('Y'));
								
								for($i = $curyear; $i >= $curyear-100; $i--){
									echo "<option value=\"{$i}\">{$i}</option>\n";
								}
							?>
						</select>
					</td>
				</tr>
				<?php
					if(!$dateIsValid){
						echo "<tr>\n";
						echo "<td/>";
						echo "<td colspan=\"2\"><span class=\"errormsg\">The specified date is invalid!</span></td>\n";
						echo "</tr>\n";
					}
					else if($age < 16){
						echo "<tr>\n";
						echo "<td/>";
						echo "<td colspan=\"2\"><span class=\"errormsg\">Sorry, you must be over 16 to sign up.</span></td>\n";
						echo "</tr>\n";
					}
				?>
				<tr>
					<td>E-Mail Address:</td>
					<td><input type="text" name="email" value="<?php echo isset($_POST['email']) ? htmlspecialchars($_POST['email']) : ''; ?>" /></td>
				</tr>
				<?php
					if(!$emailIsValid){
						echo "<tr>\n";
						echo "<td/>";
						echo "<td><span class=\"errormsg\">The email address is invalid!</span></td>\n";
						echo "</tr>\n";
					}
				?>
				<tr>
					<td>Password:</td>
					<td><input type="password" name="password" value="" /></td>
				</tr>
				<?php
					if(!$passwordIsValid){
						echo "<tr>\n";
						echo "<td/>";
						echo "<td><span class=\"errormsg\">Password is invalid. Passwords must be at least 8 characters long.</span></td>\n";
						echo "</tr>\n";
					}
				?>
				<tr>
					<td>Repeat Password:</td>
					<td><input type="password" name="passwordcheck" value="" /></td>
				</tr>
				<?php
					if(!$passwordsMatch){
						echo "<tr>\n";
						echo "<td/>";
						echo "<td><span class=\"errormsg\">The passwords don't match!</span></td>\n";
						echo "</tr>\n";
					}
				?>
			</table>
			<div id="legalNote">
				<input type="checkbox" name="legalAgreement"/>I agree to the <a href="terms_of_use.php">Terms of Use</a> and <a href="privacy_policy.php">Privacy Policy</a>
			</div>
			<div id="submitButton">
				<input type="submit" value="Register" />
			</div>
		</form>
	</div>
</div>
<?php include 'includes/footer.php'; ?>