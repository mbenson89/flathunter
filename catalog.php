<?php
	// First we execute our common code to connection to the database and start the session
	require("includes/common.php");
	require("dbAccess.php");
	
	//Read the listing type, or if the listing type is missing or invalid redirect to flatmates
	if(isset($_GET['type'])){		
		if(strtolower($_GET['type']) == 'flatmates'){
			$type = "Flatmates";
		}
		else if(strtolower($_GET['type']) == 'rentals'){
			$type = "Rental";
		}
		else{
			header("Location: catalog.php?type=flatmates");
        	die("Type invalid - redirecting to: catalog.php?type=flatmates");
		}
	}
	else{
		header("Location: catalog.php?type=flatmates");
        die("Redirecting to: catalog.php?type=flatmates");
	}
	
	if(isset($_GET['suburb'])){
		//Display suburb listings
		$suburbInfo = getSuburbInfo($_GET['suburb']);
		
		$suburbName = $suburbInfo['Suburb']['Name'];
		$suburbID = $suburbInfo['Suburb']['ID'];
		$districtName = $suburbInfo['District']['Name'];
		$districtID = $suburbInfo['District']['ID'];
		$regionName = $suburbInfo['Region']['Name'];
		$regionID = $suburbInfo['Region']['ID'];
		
		$area = $suburbName;
		unset($suburbInfo);
	}
	else if (isset($_GET['district'])){
		//Show suburb selector
		//Display district listings
		$districtInfo = getDistrictInfo($_GET['district']);
		
		$districtName = $districtInfo['District']['Name'];
		$districtID = $districtInfo['District']['ID'];
		$regionName = $districtInfo['Region']['Name'];
		$regionID = $districtInfo['Region']['ID'];
		
		$area = $districtName;
		
		unset($districtInfo);
	}
	else if (isset($_GET['region'])){
		//Show district selector
		//Display region listings
		$regionName = getRegionName($_GET['region']);
		$regionID = $_GET['region'];
		$area = $regionName;
	}
	else{
		$area = "New Zealand";
	}
	
	$pageTitle = "{$area}: {$type} Listings";
	$styles = array("page_styles/catalog.css");

 	include 'includes/header.php'; 
?>
<div id="breadcrumbs">
	<a href="/">Home</a> >
	
	<?php
		if($type == 'Flatmates'){
			if(isset($regionName)){
				echo "<a href=\"/catalog.php?type=flatmates\">Flatmates Wanted</a>";
			}
			else {
				echo "Flatmates Wanted";
			}
		}
		else if ($type == 'Rental'){
			if(isset($regionName)){
				echo "<a href=\"/catalog.php?type=rentals\">Rental Properties</a>";
			}
			else {
				echo "Rental Properties";
			}
		}
		
		
		if(isset($regionName)){
			echo " > ";
			
			if(isset($districtName)){
				echo "<a href=\"catalog.php?type={$_GET['type']}&region={$regionID}\">{$regionName}</a>";
			}
			else {
				echo $regionName;
			}
		}
		if(isset($districtName)){
			echo " > ";
			
			if(isset($suburbName)){
				echo "<a href=\"catalog.php?type={$_GET['type']}&district={$districtID}\">{$districtName}</a>";
			}
			else{
				echo $districtName;
			}
		}
		if(isset($suburbName)){
			echo " > {$suburbName}";
		}
	?>
</div>
<div id="pageHeading">
	<h1>Listings for <?php echo $area; ?></h1>
</div>
<hr>
<?php
	if(isset($_GET['region']) || isset($_GET['district']) || !isset($_GET['suburb'])){
		echo "<div id=\"catalogSubSelector\">\n
				<div id=\"catalogSubSelectorTitle\">";
		if(isset($_GET['region'])){
			echo "Districts in {$regionName}";
			$subtype = "district";
			$subItems = getDistrictsForRegion($regionID);
		}
		else if(isset($_GET['district'])){
			echo "Suburbs in {$districtName}";
			$subtype = "suburb";
			$subItems = getSuburbsForDistrict($districtID);
		}
		else{
			echo "Regions in New Zealand";
			$subtype = "region";
			$subItems = getRegions();
		}
		echo "</div>\n<ul>";
		
		foreach($subItems as $item){
			//TODO Include listing count in parentheses after region/district/suburb - eg: "Pipitea (15)", "Aro Valley (0)".
			echo "<li><a href = \"catalog.php?type={$_GET['type']}&{$subtype}={$item['ID']}\">{$item['Name']}</a></li>";
		}
		
		echo "</ul></div>";
	}
?>
<div class="catalogListing">
	
</div>
<?php
	include 'includes/footer.php';
?>
